﻿using UnityEngine;
#if UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using System.Collections;

namespace Invector.CharacterController
{
    public class vThirdPersonInput : MonoBehaviour
    {
        #region variables

        [Header("Default Inputs")]
        public string horizontalInput = "Horizontal";
        public string verticallInput = "Vertical";
        public KeyCode jumpInput = KeyCode.Space;
        public KeyCode strafeInput = KeyCode.Tab;
        public string sprintInput = "Fire2";
        public string fireInput = "Fire1"; 

        [Header("Camera Settings")]
        public string rotateCameraXInput ="Mouse X";
        public string rotateCameraYInput = "Mouse Y";

        [Header("Weapon")]
        public float RevolverDamage = 5.0f;

        [Header("Custom properties")]
        public float RotateModifier = 3.0f;
        public Transform GunTransform;

        protected vThirdPersonCamera tpCamera;                // acess camera info        
        [HideInInspector]
        public string customCameraState;                    // generic string to change the CameraState        
        [HideInInspector]
        public string customlookAtPoint;                    // generic string to change the CameraPoint of the Fixed Point Mode        
        [HideInInspector]
        public bool changeCameraState;                      // generic bool to change the CameraState        
        [HideInInspector]
        public bool smoothCameraState;                      // generic bool to know if the state will change with or without lerp  
        [HideInInspector]
        public bool keepDirection;                          // keep the current direction in case you change the cameraState

        protected vThirdPersonController cc;                // access the ThirdPersonController component   

        protected Animator _animatorComponent;
        protected AudioSource _gunAudioSFX;

        protected bool _isFireAvailable = true;

        #endregion

        protected virtual void Start()
        {
            CharacterInit();
            UpdateCameraStates();
        }

        protected virtual void CharacterInit()
        {
            cc = GetComponent<vThirdPersonController>();
            if (cc != null)
                cc.Init();

            tpCamera = FindObjectOfType<vThirdPersonCamera>();
            if (tpCamera) tpCamera.SetMainTarget(this.transform);

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;


            _animatorComponent = GetComponent<Animator>();
            _gunAudioSFX = GetComponent<AudioSource>();
        }

        protected virtual void LateUpdate()
        {
            if (cc == null) return;             // returns if didn't find the controller		    
            InputHandle();                      // update input methods
            //UpdateCameraStates();               // update camera states
        }

        protected virtual void FixedUpdate()
        {
            cc.AirControl();
            CameraInput();
        }

        protected virtual void Update()
        {
            cc.UpdateMotor();                   // call ThirdPersonMotor methods               
            cc.UpdateAnimator();                // call ThirdPersonAnimator methods		               
        }

        protected virtual void InputHandle()
        {
            if (Input.GetButton("Cancel"))
            {
                SceneManager.LoadScene("LevelCu1");
            }

            ExitGameInput();
            CameraInput();

            if (!cc.lockMovement)
            {
                if (FireInput()){

                    cc.input.x = 0;
                    cc.input.y = 0;
                    CameraInput(0);
                    cc.Sprint(false);
                    StartCoroutine(FireCountDown());

                } else
                {

                    MoveCharacter();
                    SprintInput();
                    StrafeInput();
                    JumpInput();

                }

            }
        }

        #region Basic Locomotion Inputs      

        IEnumerator FireCountDown()
        {
            _isFireAvailable = false;
            yield return new WaitForSeconds(1.5f);
            //Debug.Log("Fire Trigger");
            if (_animatorComponent != null)
            {
                _animatorComponent.SetBool("IsAttacking", false);
            }    
            _isFireAvailable = true;

        }

        protected virtual void MoveCharacter()
        {
            // cc.input.x = Input.GetAxis(horizontalInput);
            float x = Input.GetAxis(horizontalInput);
            float y = Input.GetAxis(verticallInput);

            // disable backward movement
            if (y >= 0)
            {
                cc.input.y = y;
            }
           
            CameraInput(x);

        }

        protected virtual bool FireInput()
        {

            if (!_isFireAvailable) return false;


           //Debug.Log("Fire Trigger");
            if (_animatorComponent == null) return false;
            if (!Input.GetButton(fireInput))
            {
                _animatorComponent.SetBool("IsAttacking", false);
                return false;
            }

            _animatorComponent.SetBool("IsAttacking", true);
            bool shootResult = ShootRaycast();
            return true;


        }

        protected virtual bool ShootRaycast()
        {

            if (GunTransform == null)
            {
                return false;
            }

            RaycastHit hit;
            //Transform shootTransform = transform;
            //shootTransform.SetPositionAndRotation(shootTransform.position + (new Vector3(0.0f, 5.0f, 0.0f)),
            //    shootTransform.rotation);

            //Vector3 shootModificator = new Vector3(0.0f, 2.0f, 0.0f);

           // Debug.Log(" Point A " + shootTransform.position + shootModificator);
           // Debug.Log(" Point B " + shootTransform.TransformDirection(Vector3.forward));

            // PLAY SFX
            if (_gunAudioSFX != null)
            {
                _gunAudioSFX.Play();
            }

            // PlAY WEAPON VFX

            // Does the ray intersect any objects excluding the player layer
            //Debug.Log("Debug layer : " + LayerMask.NameToLayer("EnemyLayer"));

            int mask = 1 << 9;
            // if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 1000.0f, LayerMask.NameToLayer("EnemyLayer")))
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward),out hit, 1000.0f, mask))
            {
                Debug.DrawRay(transform.position, (transform.TransformDirection(Vector3.forward)) * hit.distance, Color.green, 5, false);
                //Debug.Log("Did Hit");

               // Debug.Log("Pre HK");
     
                HealthKeeper enemyHK = hit.transform.gameObject.GetComponent<HealthKeeper>();
               // Debug.Log("Hit transform position : " + hit.transform.position);
                if (enemyHK != null)
                {
                 //   Debug.Log("We found HK");
                    enemyHK.ReceiveDamage(RevolverDamage);
                }
                //ReceiveDamage(float value);
                return true;
            }
            else
            {
                Debug.DrawRay(transform.position, (transform.TransformDirection(Vector3.forward)) * 1000, Color.red, 5, false);
                // Debug.Log("Did not Hit");
                return false;
            }


            return false;

        }

        protected virtual void StrafeInput()
        {
            if (Input.GetKeyDown(strafeInput))
                cc.Strafe();
        }

        protected virtual void SprintInput()
        {
            // if (Input.GetButton(sprintInput))
            // cc.Sprint(true);
            // else if(Input.GetKeyUp(sprintInput))
            //cc.Sprint(false);
            cc.Sprint(Input.GetButton(sprintInput));
        }

        protected virtual void JumpInput()
        {
            if (Input.GetKeyDown(jumpInput))
                cc.Jump();
        }

        protected virtual void ExitGameInput()
        {
            // just a example to quit the application 
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!Cursor.visible)
                    Cursor.visible = true;
                else
                    Application.Quit();
            }
        }

        #endregion

        #region Camera Methods

        protected virtual void CameraInput()
        {

            return;
            if (tpCamera == null)
                return;
            var Y = Input.GetAxis(rotateCameraYInput);
            var X = Input.GetAxis(rotateCameraXInput);

            tpCamera.RotateCamera(X, Y);

            // tranform Character direction from camera if not KeepDirection
            //if (!keepDirection)
                ///cc.UpdateTargetDirection(tpCamera != null ? tpCamera.transform : null);
            // rotate the character with the camera while strafing        
            //RotateWithCamera(tpCamera != null ? tpCamera.transform : null);            
        }

        protected virtual void CameraInput(float yRotation)
        {
            //Debug.Log("Horizontal input camera");
            if (tpCamera == null)
                return;

           // Debug.Log("Bypass trCamera checker");
            var X = .0f;
            var Y = yRotation;

            //Debug.Log("tCamera transform before : " + tpCamera.transform.rotation);
           // Debug.Log("xRotation : " + yRotation);
            
            tpCamera.RotateCamera(X, Y);

            //Debug.Log("Performe trCamera rotate");
            // tranform Character direction from camera if not KeepDirection

            var vTPTransform = GetComponent<Transform>();
            if (vTPTransform == null)
            {
                return;
            }

            vTPTransform.Rotate(new Vector3(.0f, 1.0f, .0f), Y * RotateModifier);
            if (!keepDirection)
            {
                //Debug.Log("Inside keep direction");
                //Debug.Log("tCamera transform after : " + tpCamera.transform.rotation);


                cc.UpdateTargetDirection(vTPTransform);

            }
            // rotate the character with the camera while strafing        
            RotateWithCamera(vTPTransform);            
        }

        protected virtual void UpdateCameraStates()
        {
            // CAMERA STATE - you can change the CameraState here, the bool means if you want lerp of not, make sure to use the same CameraState String that you named on TPCameraListData
            if (tpCamera == null)
            {
                tpCamera = FindObjectOfType<vThirdPersonCamera>();
                if (tpCamera == null)
                    return;
                if (tpCamera)
                {
                    tpCamera.SetMainTarget(this.transform);
                    tpCamera.Init();
                }
            }            
        }

        protected virtual void RotateWithCamera(Transform cameraTransform)
        {
            if (cc.isStrafing && !cc.lockMovement && !cc.lockMovement)
            {                
                cc.RotateWithAnotherTransform(cameraTransform);                
            }
        }

        #endregion     
    }
}