﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXDamageContoller : MonoBehaviour {


    public float MinSeconds = 2.0f;

    private bool _emmisionInProcess = false;
    private ParticleSystem _psStorage;
    private ParticleSystem.EmissionModule _emStorage;
    


    // Use this for initialization
    void Start () {

        _psStorage = GetComponent<ParticleSystem>();
        if (_psStorage == null)
        {
            return;
        }

        _emStorage = _psStorage.emission;
        _emStorage.enabled = false;
        _psStorage.Play();


    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartEmission()
    {
        StartEmission(MinSeconds);
    }

    public void StartEmission(float duration)
    {

        if (_emmisionInProcess)
        {
            return;
        }

        _emmisionInProcess = true;
        _emStorage.enabled = true;
        StartCoroutine(StopEmissionWithDelay(duration));


    }

    IEnumerator StopEmissionWithDelay(float duration)
    {

        yield return new WaitForSeconds(duration);
        _emStorage.enabled = false;
        _emmisionInProcess = false;

    }

  

  


}
