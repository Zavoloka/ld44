﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Assets.PSX.Src;
using Assets.PSX.Src.Interfaces;


public class AttackPerforming : MonoBehaviour {

    public string SearchTag = "Player";
    public float DamageValue = 10.0f;
    private NavMeshAgent _parentAiAgent;
    private HealthKeeper _enemyHealth;
    //private Animator _parentAnimatorComponent;

    protected AudioSource _as;

    //private ParticleSystem _ps;
    //private ParticleSystem.EmissionModule _em;

    // Use this for initialization
    void Start () {
        // _ps = GetComponent<ParticleSystem>();
        //_em = _ps.emission;    
        // _em.enabled = false;
        // _ps.Play(); 

        // Animator _parentAnimatorComponent = gameObject.GetComponentInParent(typeof(Animator)) as Animator;
        _as = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {


		
	}

    protected void OnTriggerStay(Collider other)
    {

        if (!other.CompareTag(SearchTag))
        {
            return;
        }

        if (!CheckEnemyHealthBar(other.gameObject))
        {
            return;
        }


      //  _em.enabled = true;
  
        AttackEnemy();



    }

    protected void OnTriggerExit(Collider other)
    {



        if (!other.CompareTag(SearchTag))
        {
            return;
        }

        //_enemyHealth = null;
        //_em.enabled = false;
        //_ps.Stop();
    }

    protected bool CheckEnemyHealthBar(GameObject other)
    {
        _as.Play();
        _enemyHealth = other.GetComponent<HealthKeeper>();
        if (_enemyHealth == null)
        {
            return false;
        }
        return true;  

    }

    protected void AttackEnemy()
    {
      //  if (_parentAnimatorComponent != null)
      //  {
           // _parentAnimatorComponent.SetBool("isAtacking", true);
      //  }

        _enemyHealth.ReceiveDamage(DamageValue * Time.deltaTime);
        //Debug.Log("Dealing damage to the player");
        // show some VFX/SFX feedback


       // if (_parentAnimatorComponent != null)
       // {
       //     _parentAnimatorComponent.SetBool("isAtacking", false);
       // }

    }




}
