﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollectionManager : MonoBehaviour {

    public CameraController[] Cameras;
	// Use this for initialization
	void Start () {

        if (Cameras == null) return;

        foreach (CameraController cc in Cameras)
        {
            cc.BindTriggerEvent();
        }

	}
	
}
