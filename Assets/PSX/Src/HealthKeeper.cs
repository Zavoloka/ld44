﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.PSX.Src;
using Assets.PSX.Src.Interfaces;

public class HealthKeeper : MonoBehaviour {

    public IHealth DummyHealthBar;
    public bool IsDead = false;
    public float MaxHealth = 100.0f;
    private VFXDamageContoller _vfxDamage;
    

    public delegate void DeathHandler();
    public event DeathHandler DeathEvent;



    // Use this for initialization
    void Start () {

        DummyHealthBar = new HealthBar(MaxHealth);
        _vfxDamage = GetComponent<VFXDamageContoller>();
	}
	
	// Update is called once per frame
	void Update () {

        // check health and destroy the object (simply disabled)
        if (DummyHealthBar == null) return;

        if (DummyHealthBar.GetCurrentHealth() <= 0.0f)
        {
            PerformDeath();
        }
		
	}

    public virtual void ReceiveDamage(float value)
    {

        DummyHealthBar.Decrease(value);
        _vfxDamage.StartEmission();

        var current = DummyHealthBar.GetCurrentHealth();
        //Debug.Log("Current Pawn Health : " +  current);

    }

    public virtual void PerformDeath()
    {
        IsDead = true;
        //Debug.Log("performing!");
        if (DeathEvent != null)
        {
           // Debug.Log("Almos here");
            DeathEvent();
        }
       
    }
}
