﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


namespace Assets.PSX.Src
{

    public class AIController : MonoBehaviour
    {
        public Vector3 FollowingAim;
        public BotState CurrentState = BotState.Idle;    
        public enum BotState {Idle, Following, Patroling};

        private Transform _playerTransform;
        private NavMeshAgent _nav;
        private ThirdPersonAIInputController _tpaic;
      

        // Use this for initialization
        protected void Start()
        {

            _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
            _tpaic = GetComponent<ThirdPersonAIInputController>();
            _nav = GetComponent<NavMeshAgent>();
            _nav.enabled = true;
            FollowingAim = transform.position;
        }

        // Update is called once per frame
        protected void Update()
        {


            if ((_playerTransform != null) && (_nav != null) && (_nav.enabled))
            {

                _nav.SetDestination(_playerTransform.position);
                SendInputEmulation(_nav.velocity);
            }

           // if ((_nav != null) && (_nav.enabled)) {

           //     _nav.SetDestination(FollowingAim);
           //     SendInputEmulation(_nav.velocity);
          //  }
          

            // Debug.Log(_nav.velocity);
        }

        protected void SendInputEmulation(Vector3 velocityVector)
        {

            _tpaic._xInput = velocityVector.normalized.x;
            _tpaic._yInput = velocityVector.normalized.z;
            

        }
    }


}