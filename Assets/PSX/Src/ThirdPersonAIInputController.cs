﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.CharacterController;


namespace Assets.PSX.Src
{


public class ThirdPersonAIInputController : vThirdPersonInput
{
    /*
     * Depends on : vThirdPersonController, vThirdPersonCamera
     * 
     * 
     */

    public float _xInput { get; set; }
    public float _yInput { get; set; }


    protected override void Start()
    {
            base.Start();
            _xInput = 0.0f;
            _yInput = 0.0f;

    } 

    protected override void CharacterInit()
    {
        cc = GetComponent<vThirdPersonController>();
        if (cc != null)
            cc.Init();

        //tpCamera = GameObject.Fin

        tpCamera = FindObjectOfType<vThirdPersonCamera>();
        //if (tpCamera) tpCamera.SetMainTarget(this.transform);

        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
    }

    protected override void MoveCharacter()
    {

        cc.input.x = _xInput;
        cc.input.y = _yInput;

        // TODO: always run API;
        //Debug.Log("Bot input : " + cc.input.x + " " + cc.input.y);

    }
    protected override void StrafeInput()
    {
        return;
    }

    protected override void SprintInput()
    {
        return;
    }

    protected override void JumpInput()
    {
        return;
    }

    protected override void ExitGameInput()
    {
        return;
    }

    protected override void CameraInput()
    {
        return;
    }

}


}
