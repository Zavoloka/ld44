﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Invector.CharacterController;

public class DeathController : MonoBehaviour {

    private HealthKeeper _hk;
    private vThirdPersonInput _vtpi;
    private NavMeshAgent _nva;
   // private AttackPerforming _ap;

    private bool isDeathPerformed = false;

	// Use this for initialization
	void Start () {

        _hk  = GetComponent<HealthKeeper>();
        _nva = GetComponent<NavMeshAgent>();
       

        vThirdPersonInput[] tpiComponents = GetComponents<vThirdPersonInput>();

        foreach( var component in tpiComponents)
        {
            if (component.enabled)
            {
                _vtpi = component;
                break;
            }
        }

        if ( (_hk == null) || (_vtpi == null) )
        {
            this.enabled = false;
            return;
        }

        _hk.DeathEvent += DeathReaction;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    protected virtual void DeathReaction()
    {

        // Debug.Log("hello there");
        if (isDeathPerformed)
        {
            return;
        }

        //Debug.Log("before rotation");
        if (_nva != null)
        {
            _nva.enabled = false;
        }
        gameObject.transform.Translate(new Vector3(0.0f, 5.0f, 0.0f));
        gameObject.transform.Rotate(new Vector3(0.0f, 0.0f, 90.0f));
        _vtpi.enabled = false;
        isDeathPerformed = true;

        AttackPerforming [] _ap = GetComponentsInChildren<AttackPerforming>();
        foreach (var component in _ap)
        {
            component.enabled = false;
          
        }

        // Ui reaction
        // game state

    }
}
