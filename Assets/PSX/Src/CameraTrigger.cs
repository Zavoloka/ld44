﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrigger : MonoBehaviour {

    // Event trigger
    public delegate void PlayerTrigger();
    public event PlayerTrigger onCameraTriggerEvent;

    // Use this for initialization
    void Start () {
		
	}

    // CollisionBox as Trigger should be included to GameObject
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            if (onCameraTriggerEvent != null)
            {
                // Fire camera trigger
                onCameraTriggerEvent();

            }
        
        }

    }

        // Update is called once per frame
    void Update () {
		
	}
}
