﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.RainMaker;

public class CameraController : MonoBehaviour {

    private Camera _camera;
    private CameraTrigger [] _cTrigger;

    public GameObject [] ObjectTriggers;
    public RainScript RainScriptInst;

    public void BindTriggerEvent()
    {

        _camera = GetComponent<Camera>();

        int otLenghth = ObjectTriggers.Length;
        if (otLenghth == 0) return;

        // init array with CameraTriggers
        _cTrigger = new CameraTrigger[otLenghth];

        // Loop through all CameraTriggers
        for (int i = 0; i < otLenghth; i++)
        {
            _cTrigger[i] = ObjectTriggers[i].GetComponent<CameraTrigger>();
            if (_cTrigger[i] == null) continue;

            // hooks our Switch Camera to CameraTrigger Event
            _cTrigger[i].onCameraTriggerEvent += SwitchCamera;

        }
        
    }



    // Switches main camera when player enters a trigger
    void SwitchCamera()
    {
        // check if our camera has already been enabled
        if (_camera.isActiveAndEnabled) return;

        RainScriptInst.Camera = _camera.gameObject.GetComponent<Camera>();
        _camera.gameObject.SetActive(true);

        Camera[] allCameras = Camera.allCameras;
        if (allCameras.Length == 0) return;
        

        foreach (Camera currentCamera in allCameras)
        {
            if (currentCamera.Equals(_camera)) continue;

            currentCamera.gameObject.SetActive(false);
        }
    
     


    }

}


